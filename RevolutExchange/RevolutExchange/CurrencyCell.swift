//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import UIKit

protocol CurrencyCellDelegate: AnyObject {
    func cellInputDidBecomeActive(in cell: CurrencyCell)
    func cellInputValueDidChange(in cell: CurrencyCell, to newValue: ConvertedValue)
}

class CurrencyCell: UITableViewCell {

    weak var delegate: CurrencyCellDelegate?

    @IBOutlet private var flagView: RoundedFlagView!
    @IBOutlet private var symbolLabel: UILabel!
    @IBOutlet private var nameLabel: UILabel!
    @IBOutlet private var inputField: UITextField!

    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        inputField.delegate = self
    }

    override func becomeFirstResponder() -> Bool {
        return inputField.becomeFirstResponder()
    }

    @IBAction private func didStartEditing(_ sender: UITextField) {
        if let text = sender.text, text == "0" {
            sender.text = ""
        }
        delegate?.cellInputDidBecomeActive(in: self)
    }

    @IBAction private func didChangeValue(_ sender: UITextField) {
        if let text = sender.text, let newValue = CurrencyFormatter.shared.value(from: text) {
            delegate?.cellInputValueDidChange(in: self, to: newValue)
        }
    }
}

extension CurrencyCell: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let oldText = textField.text, let oldRange = Range(range, in: oldText) else {
            return true
        }
        let newText = oldText.replacingCharacters(in: oldRange, with: string)
        return CurrencyFormatter.shared.value(from: newText) != nil || newText.isEmpty
    }
}

extension CurrencyCell {
    class func configure(cell: CurrencyCell, with dataModel: CurrencyCellDataModel) {
        cell.flagView.image = dataModel.flag
        cell.symbolLabel.text = dataModel.currencyCode
        cell.nameLabel.text = dataModel.currencyName
        cell.inputField.text = dataModel.exchangeRate
    }
}
