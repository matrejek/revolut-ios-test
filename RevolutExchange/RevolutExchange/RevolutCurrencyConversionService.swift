//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

typealias ConvertedValue = Float

class RevolutCurrencyConversionService {

    weak var delegate: ConversionServiceDelegate?

    private var convertedValue: ConvertedValue = 1.0

    private var base = Currency.default

    private let exchangeDataProvider: ExchangeDataServiceProtocol

    private var currencyToRateMap: [Currency: ExchangeRate] = [:]

    init(_ dataProvider: ExchangeDataServiceProtocol = ExchangeDataService()) {
        exchangeDataProvider = dataProvider
        exchangeDataProvider.delegate = self
    }

    private func handleProviderFailure(error: Error) {
        // kind of error handling should be added - that one is minimum, non-failing effort, not for prod use surely
        debugPrint("ExchangeDataService has failed")
        delegate?.service(self, didFailWithError: error)
    }
}

extension RevolutCurrencyConversionService: CurrencyConversionService {

    func updateConversionBase(_ newBase: Currency) {
        stop()
        let newConvertedValue = rate(for: newBase)
        convertedValue = newConvertedValue
        currencyToRateMap[newBase] = convertedValue != 0.0 ? newConvertedValue / convertedValue : 0.0
        base = newBase
        delegate?.service(self, didChangeExchangeBase: base)
        start()
    }

    func setConvertedValue(_ value: ConvertedValue) {
        convertedValue = value
        delegate?.service(self, didUpdateConvertedValue: convertedValue)
    }

    func start() {
        exchangeDataProvider.startPeriodicUpdates(with: base, interval: 1.0)
    }

    func stop() {
        exchangeDataProvider.stopPeriodicUpdates()
    }

    func rate(for currency: Currency) -> ExchangeRate {
        return (currencyToRateMap[currency] ?? 0.0) * convertedValue
    }
}

extension RevolutCurrencyConversionService: ExchangeDataServiceDelegate {

    func provider(_ provider: ExchangeDataServiceProtocol, didFailWith error: ExchangeRateLoaderError) {
        switch error {
        case .generalFailure:
            handleProviderFailure(error: error)
        case .canceled:
            return
        }
    }

    func provider(_ provider: ExchangeDataServiceProtocol, didReceive rates: ExchangeRateTable) {
        var allCurrencies = [Currency]()
        allCurrencies.append(rates.exchangeBase)
        allCurrencies.append(contentsOf: Array(rates.rates.keys))
        currencyToRateMap = rates.rates
        currencyToRateMap[rates.exchangeBase] = 1.0
        delegate?.service(self, didUpdateExchangeRates: allCurrencies, base: rates.exchangeBase)
    }
}
