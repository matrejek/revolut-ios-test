//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import UIKit

class RoundedFlagView: UIImageView {

    override func awakeFromNib() {
        super.awakeFromNib()
        clipsToBounds = true
        contentMode = .scaleAspectFill
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = bounds.height / 2
    }
}
