//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import UIKit

public class ExchangeViewController: UITableViewController {

    var presenter = ExchangeViewPresenter(RevolutCurrencyConversionService())

    weak var eventHandler: ExchangeViewEventHandler?

    private weak var _dataSource: ExchangeViewDataSource!

    private static let topmostPath = IndexPath(item: 0, section: 0)

    override public func awakeFromNib() {
        super.awakeFromNib()
        presenter.exchangeView = self
        eventHandler = presenter
    }

    override public func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        eventHandler?.handleViewLoaded()
    }

    override public func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        eventHandler?.handleCellSelected(at: indexPath.row)
    }

    override public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.numberOfRows(in: self)
    }

    override public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "CurrencyCell",
                                                       for: indexPath) as? CurrencyCell else {
            fatalError("Error while creating UI")
        }
        configure(cell: cell, for: indexPath)
        return cell
    }

    override public func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }

    private func configure(cell: CurrencyCell, for indexPath: IndexPath) {
        let cellData = dataSource.entryFor(indexPath.row, in: self)
        CurrencyCell.configure(cell: cell, with: cellData)
        cell.delegate = self
    }
}

extension ExchangeViewController: ExchangeView {

    var dataSource: ExchangeViewDataSource {
        get {
            return _dataSource
        }
        set {
            _dataSource = newValue
        }
    }

    func moveCellToTop(from oldIndex: Int) {
        let pathToMove = IndexPath(row: oldIndex, section: 0)
        guard let cell = tableView.cellForRow(at: pathToMove) else {
            return
        }
        let shouldAnimate = tableView.indexPathsForVisibleRows?.contains(pathToMove) ?? false
        if shouldAnimate {
            tableView.moveRow(at: pathToMove, to: ExchangeViewController.topmostPath)
        } else {
            UIView.performWithoutAnimation {
                tableView.moveRow(at: pathToMove, to: ExchangeViewController.topmostPath)
            }
        }
        tableView.scrollToRow(at: ExchangeViewController.topmostPath, at: .top, animated: true)
        cell.becomeFirstResponder()
    }

    func refreshData() {
        DispatchQueue.main.async {
            let pathsToRefresh = self.tableView.visibleCells.compactMap { cell -> IndexPath? in
                if let path = self.tableView.indexPath(for: cell), path.row != 0 {
                    return path
                }
                return nil
            }
            self.updateValues(at: pathsToRefresh)
        }
    }

    func reloadData() {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }

    private func updateValues(at paths: [IndexPath]) {
        paths.forEach { path in
            if let cell = self.tableView.cellForRow(at: path) as? CurrencyCell {
                self.configure(cell: cell, for: path)
            }
        }
    }
}

extension ExchangeViewController {
    public class func createController() -> ExchangeViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle(for: ExchangeViewController.self))
        // swiftlint:disable:next line_length
        guard let controller = storyboard.instantiateViewController(withIdentifier: "ExchangeViewController") as? ExchangeViewController else {
            fatalError("Unable to create ExchangeViewController instance")
        }
        return controller
    }
}

extension ExchangeViewController: CurrencyCellDelegate {
    func cellInputValueDidChange(in cell: CurrencyCell, to newValue: ConvertedValue) {
        eventHandler?.didUpdateValue(with: newValue)
    }

    func cellInputDidBecomeActive(in cell: CurrencyCell) {
        if let index = tableView.indexPath(for: cell)?.row {
            eventHandler?.handleCellSelected(at: index)
        }
    }
}
