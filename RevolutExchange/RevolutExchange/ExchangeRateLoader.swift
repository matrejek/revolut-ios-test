//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import RevolutFoundation

enum ExchangeRateLoaderError: Error {
    case canceled
    case generalFailure

    static func error(for networkError: NetworkClientError) -> ExchangeRateLoaderError {
        switch networkError {
        case .canceled:
            return .canceled
        default:
            return .generalFailure
        }
    }
}

typealias ExchangeRateLoaderCompletion = ((Result<ExchangeRateTable, ExchangeRateLoaderError>) -> Void)

protocol ExchangeRateLoaderProtocol: AnyObject {
    func loadRates(for currency: Currency, completionHandler: @escaping ExchangeRateLoaderCompletion)
}

class ExchangeRateLoader: ExchangeRateLoaderProtocol {

    private let client: NetworkClient

    private var currentTask: NetworkTaskHandleProtocol?

    init(_ networkClient: NetworkClient = HTTPClient()) {
        client = networkClient
    }

    func loadRates(for currency: Currency, completionHandler: @escaping ExchangeRateLoaderCompletion) {
        currentTask?.cancel()
        currentTask = client.task(ExchangeRateRequest(baseCurrency: currency)) { result in
            switch result {
            case .success(let response):
                if let rateTable = ExchangeRateTable(response) {
                    completionHandler(.success(rateTable))
                } else {
                    completionHandler(.failure(ExchangeRateLoaderError.generalFailure))
                }
            case .failure(let error):
                completionHandler(.failure(ExchangeRateLoaderError.error(for: error)))
            }
        }
        currentTask?.start()
    }
}
