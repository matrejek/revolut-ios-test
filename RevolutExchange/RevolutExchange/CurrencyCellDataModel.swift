//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

struct CurrencyCellDataModel {

    let flag: UIImage?
    let currencyCode: String
    let currencyName: String
    let exchangeRate: String

    init(currency: CurrencyProtocol, value: ExchangeRate) {
        flag = currency.icon
        currencyCode = currency.code
        currencyName = currency.name ?? ""
        exchangeRate = CurrencyFormatter.shared.string(from: value)
    }
}
