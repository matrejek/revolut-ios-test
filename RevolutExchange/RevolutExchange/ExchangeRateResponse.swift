//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation
import RevolutFoundation

class ExchangeRateResponse: APIResponse {
    let base: String
    let rates: [String: Float]
}
