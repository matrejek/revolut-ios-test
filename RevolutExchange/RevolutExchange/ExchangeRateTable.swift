//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

typealias ExchangeRate = Float

class ExchangeRateTable {
    let exchangeBase: Currency
    let rates: [Currency: ExchangeRate]

    init?(_ response: ExchangeRateResponse) {
        guard let base = Currency(codeString: response.base) else {
            return nil
        }
        self.exchangeBase = base
        var rates = [Currency: ExchangeRate]()
        response.rates.forEach { key, value in
            if let code = Currency(codeString: key) {
                rates[code] = value
            }
        }
        self.rates = rates
    }
}
