//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

class ExchangeViewPresenter {

    weak var exchangeView: ExchangeView!

    private var conversionService: CurrencyConversionService

    private var orderedEntries = [Currency]()

    init(_ service: CurrencyConversionService) {
        conversionService = service
        conversionService.delegate = self
    }

    private func updateBase(toItemAt index: Int) {
        var newEntriesOrder = orderedEntries
        let newBase = newEntriesOrder.remove(at: index)
        newEntriesOrder.insert(newBase, at: 0)
        orderedEntries = newEntriesOrder
        exchangeView.moveCellToTop(from: index)
    }
}

extension ExchangeViewPresenter: ExchangeViewEventHandler {

    func handleViewLoaded() {
        conversionService.start()
        exchangeView.dataSource = self
    }

    func handleCellSelected(at index: Int) {
        conversionService.updateConversionBase(orderedEntries[index])
    }

    func didUpdateValue(with newValue: ConvertedValue) {
        conversionService.setConvertedValue(newValue)
    }

    func handleViewDisappeared() {
        conversionService.stop()
    }
}

extension ExchangeViewPresenter: ExchangeViewDataSource {

    func numberOfRows(in view: ExchangeView) -> Int {
        return orderedEntries.count
    }

    func entryFor(_ index: Int, in view: ExchangeView) -> CurrencyCellDataModel {
        let value = conversionService.rate(for: orderedEntries[index])
        return CurrencyCellDataModel(currency: orderedEntries[index], value: value)
    }
}

extension ExchangeViewPresenter: ConversionServiceDelegate {

    func service(_ service: CurrencyConversionService, didUpdateConvertedValue newValue: ConvertedValue) {
        exchangeView.refreshData()
    }

    func service(_ service: CurrencyConversionService, didUpdateExchangeRates currencies: [Currency], base: Currency) {
        var ordered = currencies
        // swiftlint:disable:next force_unwrapping
        ordered.remove(at: currencies.firstIndex(of: base)!)
        ordered.insert(base, at: 0)
        // the logic here is very basic - in final version it should consider nice-looking model transition in UI
        if orderedEntries.isEmpty, orderedEntries != ordered {
            orderedEntries = ordered
            exchangeView.reloadData()
        } else {
            exchangeView.refreshData()
        }
    }

    func service(_ service: CurrencyConversionService, didChangeExchangeBase newBase: Currency) {
        if let oldBaseIndex = orderedEntries.firstIndex(of: newBase) {
            updateBase(toItemAt: oldBaseIndex)
        }
    }

    func service(_ service: CurrencyConversionService, didFailWithError error: Error) {
        // no op
    }
}
