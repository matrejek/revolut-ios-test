//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

class CurrencyFormatter: NSObject {

    static let shared = CurrencyFormatter()

    private let underlyingFormatter: NumberFormatter

    override init() {
        let formatter = NumberFormatter()
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 2
        formatter.allowsFloats = true
        formatter.locale = Locale.current
        underlyingFormatter = formatter
    }

    private func isValidValue(_ currencyString: String) -> Bool {
        if currencyString.isEmpty {
            return true
        }
        if currencyString.starts(with: "0") {
            return false
        }
        let decimalSeparator = underlyingFormatter.decimalSeparator ?? "."
        if underlyingFormatter.number(from: currencyString) != nil {
            let split = currencyString.components(separatedBy: decimalSeparator)
            let digits = split.count == 2 ? split.last ?? "" : ""
            return digits.count <= 2
        }
        return false
    }

    func string(from value: ExchangeRate) -> String {
        return underlyingFormatter.string(from: NSNumber(value: value)) ?? ""
    }

    func value(from string: String) -> ConvertedValue? {
        guard isValidValue(string) else {
            return nil
        }
        if string.isEmpty {
            return 0.0
        }
        return underlyingFormatter.number(from: string)?.floatValue
    }
}
