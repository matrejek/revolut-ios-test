//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation
import RevolutFoundation

class ExchangeRateRequest: APIRequest<ExchangeRateResponse> {

    private let base: Currency

    init(baseCurrency: Currency) {
        base = baseCurrency
        super.init(endpoint: ExchangeEndpoint.rates)
    }

    override var queryItems: [URLQueryItem] {
        var result = super.queryItems
        result.append(URLQueryItem(name: "base", value: base.code))
        return result
    }
}
