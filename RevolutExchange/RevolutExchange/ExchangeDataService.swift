//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import RevolutFoundation

class ExchangeDataService {

    private var exchangeBase: Currency!
    private var timer: Timer?
    weak var delegate: ExchangeDataServiceDelegate?
    private let ratesLoader: ExchangeRateLoaderProtocol

    init (_ loader: ExchangeRateLoaderProtocol = ExchangeRateLoader()) {
        ratesLoader = loader
    }

    @objc
    private func requestUpdate() {
        ratesLoader.loadRates(for: exchangeBase) { [weak self] result in
            switch result {
            case .success(let response):
                self?.handle(response)
            case .failure(let error):
                self?.handle(error)
            }
        }
    }

    private func handle(_ response: ExchangeRateTable) {
        delegate?.provider(self, didReceive: response)
    }

    private func handle(_ error: ExchangeRateLoaderError) {
        delegate?.provider(self, didFailWith: error)
    }
}

extension ExchangeDataService: ExchangeDataServiceProtocol {

    func startPeriodicUpdates(with newBase: Currency, interval: TimeInterval) {
        timer?.invalidate()
        exchangeBase = newBase
        requestUpdate()
        let newTimer = Timer(timeInterval: interval,
                             target: self,
                             selector: #selector(requestUpdate),
                             userInfo: nil,
                             repeats: true)
        RunLoop.main.add(newTimer, forMode: .common)
        timer = newTimer
    }

    func stopPeriodicUpdates() {
        timer?.invalidate()
        timer = nil
    }
}
