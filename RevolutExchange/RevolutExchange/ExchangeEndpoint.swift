//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import RevolutFoundation

enum ExchangeEndpoint: String, Endpoint {
    case rates = "https://revolut.duckdns.org/latest"

    var url: URL {
        // swiftlint:disable:next force_unwrapping
        return URL(string: self.rawValue)!
    }
}
