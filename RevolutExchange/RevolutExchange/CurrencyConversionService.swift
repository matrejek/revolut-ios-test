//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

protocol ConversionServiceDelegate: AnyObject {
    func service(_ service: CurrencyConversionService, didUpdateConvertedValue newValue: ConvertedValue)
    func service(_ service: CurrencyConversionService, didUpdateExchangeRates currencies: [Currency], base: Currency)
    func service(_ service: CurrencyConversionService, didChangeExchangeBase newBase: Currency)
    func service(_ service: CurrencyConversionService, didFailWithError error: Error)
}

protocol CurrencyConversionService: AnyObject {
    var delegate: ConversionServiceDelegate? { get set }
    func setConvertedValue(_ value: ConvertedValue)
    func updateConversionBase(_ newBase: Currency)
    func start()
    func stop()
    func rate(for currency: Currency) -> ExchangeRate
}
