//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import UIKit

class CountryCodeResolver: NSObject {

    private static let currencyToCountry: [Currency: String] = {
        var result = [Currency: String]()
        let availableLocales = Locale.availableIdentifiers.map { identifier -> Locale in
            let locale = Locale(identifier: identifier)
            return locale
        }
        availableLocales.forEach { locale in
            if let code = locale.currencyCode,
                let currencyCode = Currency(codeString: code),
                let region = locale.regionCode {
                result[currencyCode] = region
            }
        }
        result[Currency.default] = "EU"
        return result
    }()

    class func resolveISOCountryCode(for currency: Currency) -> String? {
        return currencyToCountry[currency]
    }
}
