//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import RevolutFoundation

protocol ExchangeDataServiceProtocol: AnyObject {
    var delegate: ExchangeDataServiceDelegate? { get set }
    func startPeriodicUpdates(with newBase: Currency, interval: TimeInterval)
    func stopPeriodicUpdates()
}

protocol ExchangeDataServiceDelegate: AnyObject {
    func provider(_ provider: ExchangeDataServiceProtocol, didFailWith error: ExchangeRateLoaderError)
    func provider(_ provider: ExchangeDataServiceProtocol, didReceive rates: ExchangeRateTable)
}
