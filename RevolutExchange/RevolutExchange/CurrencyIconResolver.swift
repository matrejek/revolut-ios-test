//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

typealias CurrencyIcon = UIImage

class CurrencyIconResolver: NSObject {
    class func iconForCurrency(_ currency: Currency) -> CurrencyIcon? {
        guard let countryCode = CountryCodeResolver.resolveISOCountryCode(for: currency),
            let image = UIImage(named: countryCode,
                                in: Bundle(for: CountryCodeResolver.self),
                                compatibleWith: nil) else {
                return nil
        }
        return image
    }
}
