//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

protocol ExchangeViewEventHandler: AnyObject {
    func handleViewLoaded()
    func handleCellSelected(at index: Int)
    func didUpdateValue(with newValue: ConvertedValue)
    func handleViewDisappeared()
}

protocol ExchangeView: AnyObject {
    var dataSource: ExchangeViewDataSource { get set }
    func moveCellToTop(from oldIndex: Int)
    func refreshData()
    func reloadData()
}

protocol ExchangeViewDataSource: AnyObject {
    func numberOfRows(in view: ExchangeView) -> Int
    func entryFor(_ index: Int, in view: ExchangeView) -> CurrencyCellDataModel
}
