//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

protocol CurrencyProtocol {
    var code: String { get }
    var name: String? { get }
    var icon: CurrencyIcon? { get }
}

struct Currency: CurrencyProtocol {

    // swiftlint:disable:next force_unwrapping
    static let `default` = Currency(codeString: "EUR")!

    static let knownCodes = Locale.isoCurrencyCodes

    let code: String

    var name: String? {
        return Locale.current.localizedString(forCurrencyCode: code)
    }

    var icon: CurrencyIcon? {
        return CurrencyIconResolver.iconForCurrency(self)
    }

    init?(codeString: String) {
        guard Currency.knownCodes.contains(codeString.uppercased()) else {
            return nil
        }
        code = codeString
    }
}

extension Currency: Hashable {
    var hashValue: Int {
        return code.hashValue
    }
}
