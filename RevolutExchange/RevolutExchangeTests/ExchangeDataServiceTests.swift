//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

// swiftlint:disable force_unwrapping force_cast

@testable import RevolutExchange
import RevolutFoundation
import XCTest

class ExchangeDataServiceTests: XCTestCase {

    func testHappyPath() {
        let ratesTable = ExchangeRateTable(ExchangeRateResponseDataFactory.validResponse)!
        let mockLoader = MockExchangeRateLoader(result: .success(ratesTable))
        let mockDelegate = MockExchangeDataServiceDelegate()
        let dataService = ExchangeDataService(mockLoader)
        dataService.delegate = mockDelegate
        dataService.startPeriodicUpdates(with: Currencies.usd, interval: 0.2)
        XCTAssertEqual(mockLoader.requestedCurrency.code, "USD")
        XCTAssertNotNil(mockDelegate.receicedResponse)
        XCTAssertEqual(mockDelegate.receicedResponse?.exchangeBase, ratesTable.exchangeBase)
        XCTAssertEqual(mockDelegate.receicedResponse?.rates, ratesTable.rates)
        let predicate = NSPredicate { object, _ -> Bool in
            let invokedenoughTimes = (object as! MockExchangeDataServiceDelegate).invocations > 4
            return invokedenoughTimes
        }
        expectation(for: predicate, evaluatedWith: mockDelegate, handler: nil)
        waitForExpectations(timeout: 1.5)
    }

    func testThatItPropagatesCancellation() {
        let mockLoader = MockExchangeRateLoader(result: .failure(.canceled))
        let mockDelegate = MockExchangeDataServiceDelegate()
        let dataService = ExchangeDataService(mockLoader)
        dataService.delegate = mockDelegate
        dataService.startPeriodicUpdates(with: Currencies.usd, interval: 0.2)
        XCTAssertEqual(mockLoader.requestedCurrency.code, "USD")
        XCTAssertEqual(mockDelegate.receivedError, .canceled)
    }

    func testThatItPropagatesErrors() {
        let mockLoader = MockExchangeRateLoader(result: .failure(.generalFailure))
        let mockDelegate = MockExchangeDataServiceDelegate()
        let dataService = ExchangeDataService(mockLoader)
        dataService.delegate = mockDelegate
        dataService.startPeriodicUpdates(with: Currencies.usd, interval: 0.2)
        XCTAssertEqual(mockLoader.requestedCurrency.code, "USD")
        XCTAssertEqual(mockDelegate.receivedError, .generalFailure)
    }
}
