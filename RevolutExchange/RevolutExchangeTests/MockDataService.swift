//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange

class MockDataService: ExchangeDataServiceProtocol {
    weak var delegate: ExchangeDataServiceDelegate?

    var started = false
    var baseCurrency: Currency?

    func startPeriodicUpdates(with newBase: Currency, interval: TimeInterval) {
        started = true
        baseCurrency = newBase
    }

    func stopPeriodicUpdates() {
        started = false
    }
}
