//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange

class MockCurrency: CurrencyProtocol {
    var code: String

    var name: String?

    var icon: CurrencyIcon?

    init(code: String, name: String?, icon: CurrencyIcon?) {
        self.code = code
        self.name = name
        self.icon = icon
    }
}
