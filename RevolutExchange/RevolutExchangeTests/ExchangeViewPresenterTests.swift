//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

// swiftlint:disable force_unwrapping

@testable import RevolutExchange
import XCTest

class ExchangeViewPresenterTests: XCTestCase {

    func testThatItStartsServiceAndSetsDataSourceOnLoad() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        XCTAssertTrue(service.started)
        XCTAssertNotNil(view.dataSource)
    }

    func testThatItStopsService() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        XCTAssertTrue(service.started)
        presenter.handleViewDisappeared()
        XCTAssertFalse(service.started)
    }

    func testThatItUpdatesBase() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        let currencies = [Currencies.usd, Currencies.pln]
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        presenter.handleCellSelected(at: 1)
        XCTAssertEqual(service.base.code, "PLN")
    }

    func testThatItReloadsAndRefreshesData() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        let currencies = [Currencies.usd, Currencies.pln]
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        XCTAssertTrue(view.reloaded)
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        XCTAssertTrue(view.refreshed)
    }

    func testThatItReturnsCorrectRowCount() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        let currencies = [Currencies.usd, Currencies.pln]
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        XCTAssertEqual(presenter.numberOfRows(in: view), 2)
    }

    func testThatItRegistersAsServiceDelegate() {
        let service = MockConversionService()
        XCTAssertNil(service.delegate)
        let presenter = ExchangeViewPresenter(service)
        XCTAssertNotNil(service.delegate)
    }

    func testThatItUpdatesConvertedValue() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        let currencies = [Currencies.usd, Currencies.pln]
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        presenter.didUpdateValue(with: 1.5)
        XCTAssertEqual(service.convertedValue, 1.5)
    }

    func testThatItRefreshesOnValueUpdate() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        let currencies = [Currencies.usd, Currencies.pln]
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        presenter.service(service, didUpdateConvertedValue: 1.4)
        XCTAssertTrue(view.refreshed)
    }

    func testThatItHandlesBaseChange() {
        let service = MockConversionService()
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        let currencies = [Currencies.usd, Currencies.pln]
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        presenter.service(service, didChangeExchangeBase: Currencies.pln)
        XCTAssertEqual(view.topCellIndex, 1)
    }

    func testThatItPassesDataToView() {
        let service = MockConversionService()
        service.rates = [Currencies.usd: 1.0, Currencies.pln: 2.0]
        let presenter = ExchangeViewPresenter(service)
        let view = MockExchangeView()
        presenter.exchangeView = view
        presenter.handleViewLoaded()
        let currencies = [Currencies.usd, Currencies.pln]
        presenter.service(service, didUpdateExchangeRates: currencies, base: currencies.first!)
        let first = presenter.entryFor(0, in: view)
        XCTAssertEqual(first.currencyCode, "USD")
        XCTAssertEqual(first.exchangeRate, CurrencyFormatter.shared.string(from: 1.0))
        let second = presenter.entryFor(1, in: view)
        XCTAssertEqual(second.currencyCode, "PLN")
        XCTAssertEqual(second.exchangeRate, CurrencyFormatter.shared.string(from: 2.0))
    }
}
