//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange

class MockConversionService: CurrencyConversionService {
    weak var delegate: ConversionServiceDelegate?

    var rates = [Currency: ExchangeRate]()

    var base = Currency.default
    var convertedValue: ConvertedValue = 0.0

    var started = false
    func setConvertedValue(_ value: ConvertedValue) {
        convertedValue = value
    }

    func updateConversionBase(_ newBase: Currency) {
        base = newBase
    }

    func start() {
        started = true
    }

    func stop() {
        started = false
    }

    func rate(for currency: Currency) -> ExchangeRate {
        return rates[currency] ?? 0.0
    }
}
