//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange

class MockExchangeView: ExchangeView {

    private var _dataSource: ExchangeViewDataSource!
    var refreshed = false
    var reloaded = false

    var topCellIndex = 0

    var dataSource: ExchangeViewDataSource {
        get {
            return _dataSource
        }
        set {
            _dataSource = newValue
        }
    }

    func moveCellToTop(from oldIndex: Int) {
        topCellIndex = oldIndex
    }

    func refreshData() {
        refreshed = true
    }

    func reloadData() {
        reloaded = true
    }
}
