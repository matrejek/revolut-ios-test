//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

// swiftlint:disable force_unwrapping

@testable import RevolutExchange

struct Currencies {
    static let usd = Currency(codeString: "USD")!
    static let pln = Currency(codeString: "PLN")!
    static let aud = Currency(codeString: "AUD")!
    static let eur = Currency(codeString: "EUR")!
}
