//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import RevolutFoundation
import XCTest

class ExchangeRateLoaderTests: XCTestCase {

    func testThatItCancelsPendingTask() {
        let first = MockTaskHandle()
        let second = MockTaskHandle()
        let client = MockHTTPClient(tasks: [first, second])
        let loader = ExchangeRateLoader(client)
        loader.loadRates(for: Currency.default) { _ in }
        loader.loadRates(for: Currency.default) { _ in }
        XCTAssertTrue(first.started)
        XCTAssertTrue(first.canceled)
        XCTAssertTrue(second.started)
        XCTAssertFalse(second.canceled)
    }

    func testThatCompletionHandlerIsInvokedForSuccess() {
        let task = MockTaskHandle()
        task.dataToReturn = ExchangeRateResponseDataFactory.validResponseData
        let client = MockHTTPClient(tasks: [task])
        let loader = ExchangeRateLoader(client)
        let completionCalled = expectation(description: "Completion was called")
        loader.loadRates(for: Currency.default) { result in
            completionCalled.fulfill()
            switch result {
            case .success(let table):
                XCTAssertNotNil(table)
            case .failure:
                XCTFail("Expected success")
            }
        }
        task.complete()
        XCTAssertTrue(task.started)
        XCTAssertFalse(task.canceled)
        waitForExpectations(timeout: 0.2)
    }

    func testThatCompletionHandlerIsInvokedForMalformedData() {
        let task = MockTaskHandle()
        task.dataToReturn = ExchangeRateResponseDataFactory.unknownBaseResponseData
        let client = MockHTTPClient(tasks: [task])
        let loader = ExchangeRateLoader(client)
        let completionCalled = expectation(description: "Completion was called")
        loader.loadRates(for: Currency.default) { result in
            completionCalled.fulfill()
            switch result {
            case .success:
                XCTFail("Expected failure")
            case .failure(let error):
                XCTAssertEqual(error, .generalFailure)
            }
        }
        task.complete()
        XCTAssertTrue(task.started)
        XCTAssertFalse(task.canceled)
        waitForExpectations(timeout: 0.2)
    }

    func testThatCompletionHandlerIsInvokedForFailure() {
        let task = MockTaskHandle()
        task.errorToReturn = .networkFailure
        let client = MockHTTPClient(tasks: [task])
        let loader = ExchangeRateLoader(client)
        let completionCalled = expectation(description: "Completion was called")
        loader.loadRates(for: Currency.default) { result in
            completionCalled.fulfill()
            switch result {
            case .success:
                XCTFail("Expected failure")
            case .failure(let error):
                XCTAssertEqual(error, .generalFailure)
            }
        }
        task.complete()
        XCTAssertTrue(task.started)
        XCTAssertFalse(task.canceled)
        waitForExpectations(timeout: 0.2)
    }

    func testThatCompletionHandlerIsInvokedForCancellation() {
        let task = MockTaskHandle()
        task.errorToReturn = .canceled
        let client = MockHTTPClient(tasks: [task])
        let loader = ExchangeRateLoader(client)
        let completionCalled = expectation(description: "Completion was called")
        loader.loadRates(for: Currency.default) { result in
            completionCalled.fulfill()
            switch result {
            case .success:
                XCTFail("Expected failure")
            case .failure(let error):
                XCTAssertEqual(error, .canceled)
            }
        }
        task.complete()
        XCTAssertTrue(task.started)
        XCTAssertFalse(task.canceled)
        waitForExpectations(timeout: 0.2)
    }
}
