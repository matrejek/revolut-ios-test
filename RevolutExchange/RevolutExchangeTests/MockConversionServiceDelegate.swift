//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange

class MockConversionServiceDelegate: ConversionServiceDelegate {

    var convertedValue: ConvertedValue = 0.0
    var base: Currency?
    var currencies = [Currency]()
    var errorReceived: Error?

    func service(_ service: CurrencyConversionService, didUpdateConvertedValue newValue: ConvertedValue) {
        convertedValue = newValue
    }

    func service(_ service: CurrencyConversionService, didUpdateExchangeRates currencies: [Currency], base: Currency) {
        self.base = base
        self.currencies = currencies
    }

    func service(_ service: CurrencyConversionService, didChangeExchangeBase newBase: Currency) {
        base = newBase
    }

    func service(_ service: CurrencyConversionService, didFailWithError error: Error) {
        errorReceived = error
    }
}
