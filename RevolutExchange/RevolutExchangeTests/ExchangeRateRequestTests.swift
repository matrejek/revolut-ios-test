//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

// swiftlint:disable force_unwrapping

@testable import RevolutExchange
import XCTest

class ExchangeRateRequestTests: XCTestCase {

    func testThatItGeneratesCorrectQuery() {
        let exchangeRequest = ExchangeRateRequest(baseCurrency: Currency(codeString: "EUR")!)
        XCTAssertNotNil(exchangeRequest)
        XCTAssertEqual(exchangeRequest.queryItems.count, 1)
        XCTAssertEqual(exchangeRequest.queryItems.first!.name, "base")
        XCTAssertEqual(exchangeRequest.queryItems.first!.value, "EUR")
    }
}
