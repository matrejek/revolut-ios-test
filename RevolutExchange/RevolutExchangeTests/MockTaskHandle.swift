//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import RevolutFoundation
import XCTest

class MockTaskHandle: NetworkTaskHandleProtocol {

    var started = false
    var canceled = false

    var errorToReturn: NetworkClientError?

    var dataToReturn: Data?

    var completionHandler: (() -> Void)?

    func start() {
        started = true
    }

    func cancel() {
        canceled = true
    }

    func complete() {
        completionHandler?()
    }
}
