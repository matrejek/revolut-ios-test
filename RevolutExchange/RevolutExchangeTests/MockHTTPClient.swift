//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import RevolutFoundation
import XCTest

class MockHTTPClient: NetworkClient {

    var tasks: [MockTaskHandle]
    var currentTask: Int = 0

    init(tasks: [MockTaskHandle]) {
        self.tasks = tasks
    }

    func task<T>(_ request: APIRequest<T>, completionHandler: @escaping ((Result<T, NetworkClientError>) -> Void)) -> NetworkTaskHandleProtocol where T: APIResponse {
        let task = tasks[currentTask]
        currentTask += 1
        task.completionHandler = {
            if let error = task.errorToReturn {
                completionHandler(.failure(error))
            } else if let data = task.dataToReturn, let response = try? JSONDecoder().decode(T.self, from: data) {
                completionHandler(.success(response))
            }
        }
        return task
    }
}
