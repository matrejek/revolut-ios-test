//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import XCTest

class CurrencyCellDataModelTests: XCTestCase {

    func testHappyPath() {
        let icon = UIImage()
        let currency = MockCurrency(code: "foo", name: "bar", icon: icon)
        let value: ExchangeRate = 1.0
        let model = CurrencyCellDataModel(currency: currency, value: value)
        XCTAssertEqual(currency.code, model.currencyCode)
        XCTAssertEqual(currency.name, model.currencyName)
        XCTAssertEqual(CurrencyFormatter.shared.string(from: value), model.exchangeRate)
        XCTAssertEqual(currency.icon, model.flag)
    }

    func testEmptyName() {
        let icon = UIImage()
        let currency = MockCurrency(code: "foo", name: nil, icon: icon)
        let value: ExchangeRate = 1.0
        let model = CurrencyCellDataModel(currency: currency, value: value)
        XCTAssertEqual(currency.code, model.currencyCode)
        XCTAssertEqual("", model.currencyName)
        XCTAssertEqual(CurrencyFormatter.shared.string(from: value), model.exchangeRate)
        XCTAssertEqual(currency.icon, model.flag)
    }
}
