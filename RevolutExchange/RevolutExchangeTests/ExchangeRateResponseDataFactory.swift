//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

// swiftlint:disable force_unwrapping force_try

@testable import RevolutExchange
import XCTest

class ExchangeRateResponseDataFactory {

    private class func data(jsonFile: String) -> Data {
        guard let filePath = Bundle(for: ExchangeDataServiceTests.self).path(forResource: jsonFile,
                                                                             ofType: "json") else {
            XCTFail("Unable to read resource data")
            fatalError("Tests are missing resources!")
        }
        guard let fileData = (try? String(contentsOfFile: filePath))?.data(using: .utf8) else {
            XCTFail("Unable to read resource data")
            fatalError("Tests resources are invalid!")
        }
        return fileData
    }

    static var validResponse: ExchangeRateResponse {
        let response = try! JSONDecoder().decode(ExchangeRateResponse.self,
                                                 from: ExchangeRateResponseDataFactory.validResponseData)
        return response
    }

    static var validResponseData: Data {
        return data(jsonFile: "validResponse")
    }

    static var unknownBaseResponseData: Data {
        return data(jsonFile: "unknownBaseResponse")
    }

    static var validRateTable: ExchangeRateTable {
        return ExchangeRateTable(validResponse)!
    }
}
