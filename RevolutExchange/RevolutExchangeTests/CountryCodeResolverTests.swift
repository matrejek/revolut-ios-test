//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import XCTest

class CountryCodeResolverTests: XCTestCase {

    func testThatItResolvesKnownValue() {
        // swiftlint:disable:next force_unwrapping
        let currency = Currency(codeString: "GBP")!
         let result = CountryCodeResolver.resolveISOCountryCode(for: currency)
        XCTAssertEqual(result, "GB")
    }

    func testThatItResolvesEuroValue() {
        // swiftlint:disable:next force_unwrapping
        let currency = Currency(codeString: "EUR")!
        let result = CountryCodeResolver.resolveISOCountryCode(for: currency)
        XCTAssertEqual(result, "EU")
    }
}
