//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import XCTest

class CurrencyFormatterTests: XCTestCase {

    func testThatItConvertsValidStringsToValues() {
        XCTAssertEqual(CurrencyFormatter.shared.value(from: "1.2"), 1.2)
        XCTAssertEqual(CurrencyFormatter.shared.value(from: "2.22"), 2.22)
        XCTAssertEqual(CurrencyFormatter.shared.value(from: "1"), 1.0)
    }

    func testThatItNotConvertsInvalidStringsToValues() {
        XCTAssertNil(CurrencyFormatter.shared.value(from: "01.2"))
        XCTAssertNil(CurrencyFormatter.shared.value(from: "aa"))
        XCTAssertNil(CurrencyFormatter.shared.value(from: "1.1.1"))
    }

    func testThatItGeneratesValidValueString() {
        XCTAssertEqual(CurrencyFormatter.shared.string(from: 1), "1")
        XCTAssertEqual(CurrencyFormatter.shared.string(from: 1.2), "1.2")
        XCTAssertEqual(CurrencyFormatter.shared.string(from: 1.25), "1.25")
        XCTAssertEqual(CurrencyFormatter.shared.string(from: 1.250), "1.25")
    }
}
