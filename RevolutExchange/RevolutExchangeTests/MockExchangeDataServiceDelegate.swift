//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

@testable import RevolutExchange
import RevolutFoundation

class MockExchangeDataServiceDelegate: ExchangeDataServiceDelegate {

    var invocations = 0
    var receicedResponse: ExchangeRateTable?
    var receivedError: ExchangeRateLoaderError?

    func provider(_ provider: ExchangeDataServiceProtocol, didFailWith error: ExchangeRateLoaderError) {
        invocations += 1
        receivedError = error
    }

    func provider(_ provider: ExchangeDataServiceProtocol, didReceive rates: ExchangeRateTable) {
        invocations += 1
        receicedResponse = rates
    }
}
