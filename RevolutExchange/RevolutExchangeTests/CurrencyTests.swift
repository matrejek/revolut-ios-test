//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import XCTest

class CurrencyTests: XCTestCase {

    func testThatItInstantiatesWithValidCode() {
        let currency = Currency(codeString: "EUR")
        XCTAssertNotNil(currency)
        XCTAssertEqual(currency?.code, "EUR")
        XCTAssertNotNil(currency?.name)
        XCTAssertNotNil(currency?.icon)
    }

    func testThatItNotInstantiatesWithInvalidCode() {
        XCTAssertNil(Currency(codeString: "UNKNOWN"))
    }
}
