//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutExchange
import RevolutFoundation

class MockExchangeRateLoader: ExchangeRateLoaderProtocol {

    var requestedCurrency: Currency!

    var resultToReturn: Result<ExchangeRateTable, ExchangeRateLoaderError>

    init(result: Result<ExchangeRateTable, ExchangeRateLoaderError>) {
        resultToReturn = result
    }

    func loadRates(for currency: Currency, completionHandler: @escaping ExchangeRateLoaderCompletion) {
        requestedCurrency = currency
        completionHandler(resultToReturn)
    }
}
