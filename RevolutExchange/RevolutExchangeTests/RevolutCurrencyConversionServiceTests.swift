//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

// swiftlint:disable force_unwrapping

@testable import RevolutExchange
import XCTest

class RevolutCurrencyConversionServiceTests: XCTestCase {

    func testThatItStartsDataService() {
        let dataService = MockDataService()
        let service = RevolutCurrencyConversionService(dataService)
        service.updateConversionBase(Currencies.usd)
        service.start()
        XCTAssertTrue(dataService.started)
        XCTAssertEqual(dataService.baseCurrency, Currencies.usd)
    }

    func testThatItStopsDataService() {
        let dataService = MockDataService()
        let service = RevolutCurrencyConversionService(dataService)
        service.updateConversionBase(Currencies.usd)
        service.start()
        XCTAssertTrue(dataService.started)
        service.stop()
        XCTAssertFalse(dataService.started)
    }

    func testThatItNotifiesDelegateWithNewData() {
        let dataService = MockDataService()
        let delegate = MockConversionServiceDelegate()
        let rateTable = ExchangeRateResponseDataFactory.validRateTable
        let service = RevolutCurrencyConversionService(dataService)
        service.delegate = delegate
        service.provider(dataService, didReceive: rateTable)
        XCTAssertEqual(delegate.currencies.count, 2)
        XCTAssertTrue(delegate.currencies.contains(Currencies.eur))
        XCTAssertTrue(delegate.currencies.contains(Currencies.aud))
        XCTAssertEqual(delegate.base, Currencies.eur)
    }

    func testThatItCalculatesExchangeRates() {
        let dataService = MockDataService()
        let delegate = MockConversionServiceDelegate()
        let rateTable = ExchangeRateResponseDataFactory.validRateTable
        let service = RevolutCurrencyConversionService(dataService)
        service.delegate = delegate
        service.provider(dataService, didReceive: rateTable)
        let baseRate: ExchangeRate = 1.0
        XCTAssertEqual(service.rate(for: rateTable.exchangeBase), baseRate)
        XCTAssertEqual(service.rate(for: Currencies.aud), rateTable.rates[Currencies.aud])
        let convertedValue: ConvertedValue = 2.0
        service.setConvertedValue(convertedValue)
        XCTAssertEqual(service.rate(for: rateTable.exchangeBase), baseRate * convertedValue)
        XCTAssertEqual(service.rate(for: Currencies.aud), (rateTable.rates[Currencies.aud]!) * convertedValue)
    }

    func testThatItPropagatesFailures() {
        let dataService = MockDataService()
        let delegate = MockConversionServiceDelegate()
        let service = RevolutCurrencyConversionService(dataService)
        service.delegate = delegate
        service.provider(dataService, didFailWith: .generalFailure)
        XCTAssertNotNil(delegate.errorReceived)
    }

    func testThatItNotPropagatesCancelation() {
        let dataService = MockDataService()
        let delegate = MockConversionServiceDelegate()
        let service = RevolutCurrencyConversionService(dataService)
        service.delegate = delegate
        service.provider(dataService, didFailWith: .canceled)
        XCTAssertNil(delegate.errorReceived)
    }
}
