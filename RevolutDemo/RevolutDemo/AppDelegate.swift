//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import RevolutExchange
import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let window = UIWindow(frame: UIScreen.main.bounds)
        let navController = UINavigationController(rootViewController: ExchangeViewController.createController())
        navController.navigationBar.barTintColor = .white
        navController.navigationBar.isTranslucent = false
        window.rootViewController = navController
        window.makeKeyAndVisible()
        self.window = window
        return true
    }
}
