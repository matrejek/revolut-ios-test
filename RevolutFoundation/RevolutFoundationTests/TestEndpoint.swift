//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutFoundation

enum TestEndpoint: String, Endpoint {
    case test = "https://foo.bar"

    var url: URL {
        // swiftlint:disable:next force_unwrapping
        return URL(string: self.rawValue)!
    }
}
