//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutFoundation
import XCTest

class NetworkTaskHandleTests: XCTestCase {

    private class TaskMock: NetworkTask {

        var started = false
        var canceled = false

        func resume() {
            started = true
        }

        func cancel() {
            canceled = true
        }
    }

    func testThatItStartsTask() {
        let task = TaskMock()
        let handle = NetworkTaskHandle(task)
        handle.start()
        XCTAssertTrue(task.started)
    }

    func testThatItCancelsTask() {
        let task = TaskMock()
        let handle = NetworkTaskHandle(task)
        handle.cancel()
        XCTAssertTrue(task.canceled)
    }
}
