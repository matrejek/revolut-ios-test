//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutFoundation

class MockResponse: APIResponse, Equatable, Encodable {
    var foo: String = "bar"

    static func == (lhs: MockResponse, rhs: MockResponse) -> Bool {
        return lhs.foo == rhs.foo
    }
}
