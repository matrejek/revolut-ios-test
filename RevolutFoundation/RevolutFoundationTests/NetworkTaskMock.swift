//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutFoundation

class NetworkTaskMock: NetworkTask {

    var response: HTTPURLResponse?

    var error: Error?

    var data: Data?

    var completionHandler: NetworkTaskCompletion!

    func resume() {
        completionHandler(data, response, error)
    }

    func cancel() {}
}
