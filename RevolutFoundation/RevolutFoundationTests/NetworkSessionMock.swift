//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutFoundation

class NetworkSessionMock: NetworkSession {

    private let task: NetworkTaskMock

    init(taskMock: NetworkTaskMock) {
        task = taskMock
    }

    func networkTask(with request: URLRequest,
                     completionHandler: @escaping (Data?, URLResponse?, Error?) -> Void) -> NetworkTask {
        task.completionHandler = completionHandler
        return task
    }
}
