//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

@testable import RevolutFoundation
import XCTest

class MockRequest: APIRequest<MockResponse> {}

class MockError: Error, Equatable {
    static func == (lhs: MockError, rhs: MockError) -> Bool {
        return true
    }
}

class HTTPClientTests: XCTestCase {

    func testThatItCallsCompletionForNetworkError() {
        let task = NetworkTaskMock()
        let error = MockError()
        task.error = error
        let session = NetworkSessionMock(taskMock: task)
        let request = MockRequest(endpoint: TestEndpoint.test)
        let client = HTTPClient(session)
        let expectaction = expectation(description: "Completion called")
        let networkTask = client.task(request) { result in
            XCTAssertEqual(result, Result.failure(NetworkClientError.networkFailure))
            expectaction.fulfill()
        }
        networkTask.start()
        waitForExpectations(timeout: 1.0)
    }

    func testThatItCallsCompletionForMissingData() {
        let task = NetworkTaskMock()
        task.response = HTTPURLResponse(url: TestEndpoint.test.url,
                                        statusCode: 200,
                                        httpVersion: nil,
                                        headerFields: nil)
        let session = NetworkSessionMock(taskMock: task)
        let request = MockRequest(endpoint: TestEndpoint.test)
        let client = HTTPClient(session)
        let expectaction = expectation(description: "Completion called")
        let networkTask = client.task(request) { result in
            XCTAssertEqual(result, Result.failure(NetworkClientError.missingData))
            expectaction.fulfill()
        }
        networkTask.start()
        waitForExpectations(timeout: 1.0)
    }

    func testThatItCallsCompletionForMissingResponse() {
        let task = NetworkTaskMock()
        task.response = nil
        let session = NetworkSessionMock(taskMock: task)
        let request = MockRequest(endpoint: TestEndpoint.test)
        let client = HTTPClient(session)
        let expectaction = expectation(description: "Completion called")
        let networkTask = client.task(request) { result in
            XCTAssertEqual(result, Result.failure(NetworkClientError.networkFailure))
            expectaction.fulfill()
        }
        networkTask.start()
        waitForExpectations(timeout: 1.0)
    }

    func testThatItCallsCompletionForInvalidStatusCode() {
        let task = NetworkTaskMock()
        let session = NetworkSessionMock(taskMock: task)
        let response = HTTPURLResponse(url: TestEndpoint.test.url, statusCode: 404, httpVersion: nil, headerFields: nil)
        task.response = response
        let request = MockRequest(endpoint: TestEndpoint.test)
        let client = HTTPClient(session)
        let expectaction = expectation(description: "Completion called")
        let networkTask = client.task(request) { result in
            XCTAssertEqual(result, Result.failure(NetworkClientError.invalidResponse(code: 404)))
            expectaction.fulfill()
        }
        networkTask.start()
        waitForExpectations(timeout: 1.0)
    }

    func testThatItCallsCompletionForDecoderFailure() {
        let task = NetworkTaskMock()
        task.response = HTTPURLResponse(url: TestEndpoint.test.url,
                                        statusCode: 200,
                                        httpVersion: nil,
                                        headerFields: nil)
        // swiftlint:disable:next force_unwrapping
        task.data = "foo".data(using: .utf8)!
        let session = NetworkSessionMock(taskMock: task)
        let request = MockRequest(endpoint: TestEndpoint.test)
        let client = HTTPClient(session, decoder: FailingDecoder())
        let expectaction = expectation(description: "Completion called")
        let networkTask = client.task(request) { result in
            XCTAssertEqual(result, Result.failure(NetworkClientError.invalidData))
            expectaction.fulfill()
        }
        networkTask.start()
        waitForExpectations(timeout: 1.0)
    }

    func testThatItCallsCompletionForSuccess() {
        let task = NetworkTaskMock()
        task.response = HTTPURLResponse(url: TestEndpoint.test.url,
                                        statusCode: 200,
                                        httpVersion: nil,
                                        headerFields: nil)
        let response = MockResponse()
        task.data = try? JSONEncoder().encode(response)
        let session = NetworkSessionMock(taskMock: task)
        let request = MockRequest(endpoint: TestEndpoint.test)
        let client = HTTPClient(session)
        let expectaction = expectation(description: "Completion called")
        let networkTask = client.task(request) { result in
            XCTAssertEqual(result, Result.success(response))
            expectaction.fulfill()
        }
        networkTask.start()
        waitForExpectations(timeout: 1.0)
    }
}

class FailingDecoder: ResponseDecoder {

    func decodeResponse<R>(ofType: R.Type, from data: Data) -> R? where R: APIResponse {
        return nil
    }
}
