//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

open class APIRequest<R: APIResponse> {
    private let endpointURL: URL
    let requestID: String

    public init(endpoint: Endpoint) {
        endpointURL = endpoint.url
        requestID = UUID().uuidString
    }

    open var queryItems: [URLQueryItem] {
        return []
    }

    var httpRequest: URLRequest {
        // swiftlint:disable:next force_unwrapping
        var urlComponents = URLComponents(url: endpointURL, resolvingAgainstBaseURL: false)!
        urlComponents.queryItems = queryItems
        // swiftlint:disable:next force_unwrapping
        return URLRequest(url: urlComponents.url!)
    }
}
