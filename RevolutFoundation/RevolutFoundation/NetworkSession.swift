//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

public typealias NetworkTaskCompletion = (Data?, URLResponse?, Error?) -> Void

public protocol NetworkSession: AnyObject {
    func networkTask(with request: URLRequest, completionHandler: @escaping NetworkTaskCompletion) -> NetworkTask
}

extension URLSession: NetworkSession {
    public func networkTask(with request: URLRequest,
                            completionHandler: @escaping NetworkTaskCompletion) -> NetworkTask {
        return dataTask(with: request, completionHandler: completionHandler)
    }
}
