//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

extension JSONDecoder: ResponseDecoder {
    public func decodeResponse<R: APIResponse>(ofType: R.Type, from data: Data) -> R? {
        return try? decode(R.self, from: data)
    }
}
