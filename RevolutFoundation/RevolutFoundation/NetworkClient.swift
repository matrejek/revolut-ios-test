//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

public protocol NetworkClient: AnyObject {
    func task<T>(_ request: APIRequest<T>,
                 completionHandler: @escaping ((Result<T, NetworkClientError>) -> Void)) -> NetworkTaskHandleProtocol
}
