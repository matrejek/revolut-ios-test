//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

public class HTTPClient: NetworkClient {

    private let underlyingSession: NetworkSession

    private let responseDecoder: ResponseDecoder

    public init(_ networkSession: NetworkSession = URLSession.shared, decoder: ResponseDecoder = JSONDecoder()) {
        underlyingSession = networkSession
        responseDecoder = decoder
    }

    public func task<T>(_ request: APIRequest<T>, completionHandler: @escaping ((Result<T, NetworkClientError>) -> Void)) -> NetworkTaskHandleProtocol {
        let task = underlyingSession.networkTask(with: request.httpRequest) { data, response, error in
            guard error == nil else {
                if let nsError = error as? NSError, nsError.code == NSURLErrorCancelled {
                    completionHandler(.failure(.canceled))
                } else {
                    completionHandler(.failure(.networkFailure))
                }
                return
            }
            guard let httpResponse = response as? HTTPURLResponse else {
                completionHandler(.failure(.networkFailure))
                return
            }
            guard httpResponse.statusCode == 200 else {
                completionHandler(.failure(.invalidResponse(code: httpResponse.statusCode)))
                return
            }
            guard let data = data else {
                completionHandler(.failure(.missingData))
                return
            }
            guard let response = self.responseDecoder.decodeResponse(ofType: T.self, from: data) else {
                completionHandler(.failure(.invalidData))
                return
            }
            completionHandler(.success(response))
        }
        return NetworkTaskHandle(task)
    }
}
