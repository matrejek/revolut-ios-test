//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

// This is a minimaistic backport of Swift 5 Result type

public enum Result<Success, Failure: Error> {
    case success(Success)
    case failure(Failure)
}

extension Result: Equatable where Success: Equatable, Failure: Equatable { }

extension Result: Hashable where Success: Hashable, Failure: Hashable { }
