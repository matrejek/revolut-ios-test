//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

public protocol ResponseDecoder: AnyObject {
    func decodeResponse<R: APIResponse>(ofType: R.Type, from data: Data) -> R?
}
