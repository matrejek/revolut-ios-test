//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

public protocol NetworkTaskHandleProtocol {
    func start()
    func cancel()
}

public class NetworkTaskHandle: NetworkTaskHandleProtocol {

    private let underlyingTask: NetworkTask

    init(_ underlyingTask: NetworkTask) {
        self.underlyingTask = underlyingTask
    }

    public func start() {
        underlyingTask.resume()
    }

    public func cancel() {
        underlyingTask.cancel()
    }
}
