//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

public protocol NetworkTask: AnyObject {
    func resume()
    func cancel()
}

extension URLSessionDataTask: NetworkTask {}
