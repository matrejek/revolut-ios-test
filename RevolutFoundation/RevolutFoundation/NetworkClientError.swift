//
//  Created by Mateusz Matrejek
//  Copyright © 2019 Mateusz Matrejek. All rights reserved.
//

import Foundation

public enum NetworkClientError: Error, Equatable {
    case badRequest
    case networkFailure
    case invalidResponse(code: Int)
    case missingData
    case invalidData
    case canceled
}
